# Ruby 15-11 Hamming code validator Example

[Inspired by this 3Blue1Brown video here](https://youtu.be/X8jsijhllIA)

Given an array of 1's and 0's of length 16, it will verify whether the
data was corrupted.
It will display if there are no errors, at what index the error is at,
or report if there are >2 errors found.

[There is a follow-up video here](https://youtu.be/b3NxrZOu_CE)

This is where some binary logic can be applied to get a the answer faster
I wrote two methods based off of this video:
  1.  That manually sums up the index of the error by relying on the flags
  2.  That's a replication of the 1 liner described towards the end of this video,
      which uses XOR to check if the message is corrupted

~~Neither of the latter two methods can handle >1 error correctly, nor can they
tell if the bit at index 0 was flipped. These would need additional checks that I have not coded.~~ Implemented for the former.

This is not meant to be used as a library, instead just as an example.