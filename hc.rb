# A Ruby 15-11 Hamming code validator
#
# Inspired by this 3Blue1Brown video here: https://youtu.be/X8jsijhllIA
#
# Given an array of 1's and 0's of length 16, it will verify whether the
# data was corrupted.
# It will display if there are no errors, at what index the error is at,
# or report if there are >2 errors found.
#
# There is a follow-up video here: https://youtu.be/b3NxrZOu_CE
# This is where some binary logic can be applied to get a the answer faster
# I wrote two methods based off of this video:
#   1:  that manually sums up the index of the error by relying on the flags
#   2:  that's a replication of the 1 liner described towards the end of this video,
#       which uses XOR to check if the message is corrupted
# Neither of these two methods can handle >1 error correctly, nor can they
# tell if the bit at index 0 was flipped
# These would need additional checks that I don't feel like writing at this moment

class HammingCodeValidator
  def initialize
    @block_indexes = (1..15).to_a
    @col_indexes = [3,5,7,9,11,13,15]
    @right_indexes = [3,6,7,10,11,14,15]
    @row_indexes = [5,6,7,12,13,14,15]
    @bottom_indexes = [9, 10, 11, 12, 13, 14, 15]
  end

  def isRangeEven(indexes, msg)
    count = 0
    indexes.map {|i| count += 1 if msg[i] == true}

    return true if count % 2 == 0
    return false
  end

  # BEGIN NESTED-IF SOLUTION
  # This is not a good way to go about it
  # You will get the correct answers, but at what cost
  def findError(msg)
    return "[Error: input must be a multiple of 16]" if msg.length != 16

    # flags
    block = false
    col = false
    right = false
    row = false
    bottom = false

    block = msg[0] ^ isRangeEven(@block_indexes, msg)
    col = msg[1] ^ isRangeEven(@col_indexes, msg)
    right = msg[2] ^ isRangeEven(@right_indexes, msg)
    row = msg[4] ^ isRangeEven(@row_indexes, msg)
    bottom = msg[8] ^ isRangeEven(@bottom_indexes, msg)

    # I suffered so you don't have to
    if !block
      if !col
        if !right
          if !row
            if !bottom
              return 15
            else
              return 7
            end
          else
            if !bottom
              return 11
            else
              return 3
            end
          end
        else
          if !row
            if !bottom
              return 13
            else
              return 5
            end
          else
            if !bottom
              return 9
            else
              return 1
            end
          end
        end
      else
        if !right
          if !row
            if !bottom
              return 14
            else
              return 6
            end
          else
            if !bottom
              return 10
            else
              return 2
            end
          end
        else
          if !row
            if !bottom
              return  12
            else
              return 4
            end
          else
            if !bottom
              return 8
            else
              return 0
            end
          end
        end
      end
    end

    return "No Error found" if col & right & row & bottom & block == true
    return '2 errors found'

  end

  # BEGIN SIMPLIFIED
  # The version using a trick of the binary, utilizing
  # the flags and previously written methods to determine
  # what to add
  def findErrorSimplified(msg)
    return "[Error: input must be a multiple of 16]" if msg.length != 16


    # flags
    block = false
    col = false
    right = false
    row = false
    bottom = false

    block = msg[0] ^ isRangeEven(@block_indexes, msg)
    col = msg[1] ^ isRangeEven(@col_indexes, msg)
    right = msg[2] ^ isRangeEven(@right_indexes, msg)
    row = msg[4] ^ isRangeEven(@row_indexes, msg)
    bottom = msg[8] ^ isRangeEven(@bottom_indexes, msg)

    output = 0
    if !col
      output += 1
    end
    if !right
      output += 2
    end
    if !row
      output += 4
    end
    if !bottom
      output += 8
    end

    return "No Error found" if col & right & row & bottom & block == true
    return "2 Errors" if col & right & row & bottom != block
    return output
  end

  # BEGIN XOR
  # A re-application of the XOR method described at the end
  # of the second video linked above.
  # In practice, this accomplishes the same thing as the binary trick solution above
  # But instead of it being more verbose, it instead utilizes XOR to trick the index
  # out of the input data
  # A benefit of this method is that it doesn't have to rely on the input data
  # being of length 16, and instead you can scale it up so long as the length
  # of the input data matches a a binary base >= 16, ie. 16, 32, 64, 128, etc.
  def findErrorXOR(msg)
    return "Error, input must be a multiple of 16" if msg.length % 16 != 0
    bits = []
    msg.each_with_index.map {|e,i| bits << i if e }
    
    return bits.reduce(:^)
  end

end