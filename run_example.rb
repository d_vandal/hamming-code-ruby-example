require './hc.rb'

# just converts the input from 1's and 0's into true and false
def processMsg(msg)
  output = []
  msg.each do |i|
    if i == 0
      output << true
    else
      output << false
    end
  end

  return output
end

# Input data set
messages = [
  [1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0], # untainted
  [0,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0],
  [1,1,1,1,0,1,1,1,0,0,0,1,0,0,1,0],
  [1,0,0,1,0,1,1,1,0,0,0,1,0,0,1,0],
  [1,0,1,0,0,1,1,1,0,0,0,1,0,0,1,0],
  [1,0,1,1,1,1,1,1,0,0,0,1,0,0,1,0],
  [1,0,1,1,0,0,1,1,0,0,0,1,0,0,1,0],
  [1,0,1,1,0,1,0,1,0,0,0,1,0,0,1,0],
  [1,0,1,1,0,1,1,0,0,0,0,1,0,0,1,0],
  [1,0,1,1,0,1,1,1,1,0,0,1,0,0,1,0],
  [1,0,1,1,0,1,1,1,0,1,0,1,0,0,1,0],
  [1,0,1,1,0,1,1,1,0,0,1,1,0,0,1,0],
  [1,0,1,1,0,1,1,1,0,0,0,0,0,0,1,0],
  [1,0,1,1,0,1,1,1,0,0,0,1,1,0,1,0],
  [1,0,1,1,0,1,1,1,0,0,0,1,0,1,1,0],
  [1,0,1,1,0,1,1,1,0,0,0,1,0,0,0,0],
  [1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,1],
  [1,0,1,1,0,1,1,1,0,0,0,1,0,0,0,1],      # 2 errors
  [1,0,1,1,0,1,1,1,0,0,0,1,0,1,0,1],      # 3 errors, yields improper result, outside the capabilities of a Hamming code
  [1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,1, 1],   # extra input
]


hc = HammingCodeValidator.new

print "Horrific nested ifs\n"
messages.each do |msg|
  print hc.findError(processMsg(msg)).to_s, ", "
end

print "\n------------------------\n"
print "Simplified\n"

messages.each do |msg|
  print hc.findErrorSimplified(processMsg(msg)).to_s, ", "
end

print "\n------------------------\n"
print "XOR\n"
messages.each do |msg|
  print hc.findErrorXOR(processMsg(msg)).to_s, ", "
end

msg16 = processMsg([1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0])
msg32 = processMsg([1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0])
msg64 = processMsg([1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0,1,0,1,1,0,1,1,1,0,0,0,1,0,0,1,0])

print "\n\nXOR examples with other lengths, showing the scalability of it."
print "\nAll of these will return 0, as there are no errors.\n"
print "Edit the input to see it adjust based on the index you change.\n\n"

print "Message length 32: ", hc.findErrorXOR(msg16), "\n"

print "Message length 64: ", hc.findErrorXOR(msg32), "\n"

print "Message length 128: ", hc.findErrorXOR(msg64), "\n"

print "\nFinished\n"


